import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { NotfoundComponent } from './pages/error/notfound/notfound.component';
import { BillComponent } from './pages/bill/bill.component';
import { IssuesComponent } from './pages/issues/issues.component';
import { AccessoriesComponent } from './pages/accessories/accessories.component';
import { ConditionComponent } from './pages/condition/condition.component';
import { OverallConditionComponent } from './pages/overall-condition/overall-condition.component';
import { FinalFormComponent } from './pages/final-form/final-form.component';
import { FinalErrorComponent } from './pages/final-error/final-error.component';
import { SeriesPageComponent } from './pages/series-page/series-page.component';
import { VersionPageComponent } from './pages/version-page/version-page.component';
import { ThankyouComponent } from './pages/thankyou/thankyou.component';
import { ContactComponent } from './pages/contact/contact.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'issues',
    component: IssuesComponent,
  },
  {
    path: 'accessories',
    component: AccessoriesComponent,
  },
  {
    path: 'conditions',
    component: ConditionComponent,
  },
  {
    path: 'overall-conditions',
    component: OverallConditionComponent,
  },
  {
    path: 'final-form',
    component: FinalFormComponent,
  },
  {
    path: 'final-error',
    component: FinalErrorComponent,
  },
  {
    path: 'thank-you',
    component: ThankyouComponent,
    pathMatch: 'full'

  },
  { path: '404', component: NotfoundComponent },

  {
    path: 'contact',
    component: ContactComponent,
  },
  {
    path: 'about-us',
    component: AboutUsComponent,
  },
  {
    path: ':brand',
    component: SeriesPageComponent
  },
  {
    path: ':brand/:series',
    component: VersionPageComponent,
  },
  {
    path: ':brand/:series/:model',
    component: BillComponent
  },
  { path: '**', redirectTo: '404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
