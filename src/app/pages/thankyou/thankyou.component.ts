import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css']
})
export class ThankyouComponent implements OnInit {
  model: string;
  constructor(public helper: HelperService) { }

  ngOnInit() {
    this.model = localStorage.getItem('model');
  }

}
