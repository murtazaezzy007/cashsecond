import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  form: FormGroup;

  constructor(public api: ApiService, public router: Router) { 
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required]),
      mobile: new FormControl('', [Validators.required]),
      text: new FormControl('', [Validators.required])
    });
  }

  ngOnInit() {
  }

  submit() {
    console.log(this.form.value)
    const formdata = new FormData();
    Object.keys(this.form.value).forEach(i => {
      formdata.append(i, this.form.value[i]);
    });

    this.api.postContactData(formdata).subscribe(
      res => {
        console.log(res);
        // if (res && res['status']) {
          // this.router.navigate(['thank-you']);
        // } else {
          alert('Thank You. We will Reply you soon.');
          this.router.navigate(['/']);

        // }
      },
      err => {
        console.log(err);
        alert('Sorry! Your request could not be completed.');
      }
    );
  }
}
