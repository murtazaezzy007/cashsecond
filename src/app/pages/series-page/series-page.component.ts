import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-series-page',
  templateUrl: './series-page.component.html',
  styleUrls: ['./series-page.component.css']
})
export class SeriesPageComponent implements OnInit {
  series: any = [];

  constructor(private route: ActivatedRoute, public api: ApiService, private helper: HelperService, public router: Router) {
    this.route.params.subscribe((params: any) => {
      // console.log(params);

      if (params && params.brand) {
        // console.log('Brand ID: ', params.brand);

        this.api.getSeries({brand: params.brand}).subscribe(res=>{
          console.log(res)

          if(res == 'no') {
            this.router.navigate(['404'])            
          }
          else {
            this.series = res;
          }
        },err=>{
          console.log(err);
        })
      } else {
        console.error('No BID received in URL');
      }
    });
  }

  ngOnInit() {
    this.helper.showFooter();
  }

}