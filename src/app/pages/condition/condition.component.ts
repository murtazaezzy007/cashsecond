import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-condition',
  templateUrl: './condition.component.html',
  styleUrls: ['./condition.component.css']
})
export class ConditionComponent implements OnInit {
  conditionsArray: any = [];
  model;
  constructor(public router: Router, public helper: HelperService) { }

  ngOnInit() {
    this.helper.hideFooter();
    this.initArray();
    this.model = localStorage.getItem('model');
  }
  
  initArray() {
    this.conditionsArray = [
      {
        name: 'Display/Touchpad Issue/Discoloration',
        class: '',
        src: 'assets/img/touch.png',
        alt: 'display_touched_issue_discoloration',
        status: false
      },
      {
        name: 'Screen Glass Broken',
        class: '',
        src: 'assets/img/broken.png',
        alt: 'screen_broker',
        status: false
      },
      {
        name: 'Back Camera Not Working',
        class: '',
        src: 'assets/img/back-camera.png',
        alt: 'back_camera',
        status: false
      },
      {
        name: ' Battery Faulty',
        class: '',
        src: 'assets/img/battery.png',
        alt: 'battery_issue',
        status: false
      },
      {
        name: 'Volume / Power / Home button defect',
        class: '',
        src: 'assets/img/volume.png',
        alt: 'volume_power_button',
        status: false
      },
      {
        name: 'WIFI/GPS not working',
        class: '',
        src: 'assets/img/wifi.png',
        alt: 'gps_wifi_issue',
        status: false
      },
      {
        name: 'Speaker Not Working',
        class: '',
        src: 'assets/img/speaker.png',
        alt: 'speaker',
        status: false
      },
      {
        name: 'Charging Defect',
        class: '',
        src: 'assets/img/charger.png',
        alt: 'charging_defect',
        status: false
      },
      {
        name: 'Front Camera Not Working',
        class: '',
        src: 'assets/img/camera.png',
        alt: 'front_camera',
        status: false
      },
      {
        name: 'Fingertouch Not Working',
        class: '',
        src: 'assets/img/home.png',
        alt: 'finger_touch_issue',
        status: false
      },
    ]
  }

  getCondition(data) {
    if(data.status == true) 
      data.status = false;
    else if(data.status == false)
      data.status = true;
  }

  navigate() {
    var condition = [];
    this.conditionsArray.forEach((element,index) => {
      if(element.status == true) {
        condition.push(element.alt);
        if(element.alt == 'display_touched_issue_discoloration' || element.alt == 'screen_broker') {
          localStorage.setItem('bill',JSON.stringify(4));
        }
      }

      if(index == this.conditionsArray.length - 1) {
        // console.log(condition);
        localStorage.setItem('condition',JSON.stringify(condition));
        localStorage.setItem('model' , this.model);
        this.router.navigate(['accessories']);
      }
    });
  }
}