import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-overall-condition',
  templateUrl: './overall-condition.component.html',
  styleUrls: ['./overall-condition.component.css']
})
export class OverallConditionComponent implements OnInit {
  conditionsArray: any = [];
  model;
  constructor(public router: Router, public api: ApiService, public helper: HelperService) { }

  ngOnInit() {
    this.helper.hideFooter();
    this.model = localStorage.getItem('model');
    this.conditionsArray = [
      {
        title: 'Excellent',
        alt: 'excellent',
        first: 'No scratches',
        second: 'No signs of usage',
        third: 'No dents or cracks on panel',
        // fourth: ''
      },
      {
        title: 'Good',
        alt: 'good',
        first: 'minor scratches',
        second: 'normal signs of usage',
        third: 'external housing intact ; no broken part',
        fourth: 'no dents or cracks on panel'
      },
      {
        title: 'Average',
        alt: 'average',
        first: 'scratches or dent on panel',
        second: 'discoloration of the panel',
        third: 'external housing intact ; minor cracks',
        // fourth: ''
      },
      {
        title: 'Below average',
        alt: 'below_average',
        first: 'multiple dent or scratches',
        second: 'panel broken',
        third: 'totally in rough condition',
        // fourth: ''
      },
    ]
  }

  navigate(title) {
    var accessory = JSON.parse(localStorage.getItem('accessory'));
    var condition = JSON.parse(localStorage.getItem('condition'));

    var totalArray = [
      ...accessory,
      ...condition
    ]

    totalArray.push(title);

    // console.log(totalArray);

    this.api.getSingleModel({month: localStorage.getItem('bill'), modal: localStorage.getItem('modal')}).subscribe(res=>{
      var json = {};
      if(res == 'no') {
        json['upto_price'] = 0
      }
      else {
        json = res[0];
        json['upto_price'] = Number(json['upto_price']);
      }
      

      localStorage.removeItem('g_id');
      localStorage.setItem('g_id', res[0].g_id);

      var expend = 0;
      Object.keys(json).forEach(data=>{
        totalArray.forEach(total=>{
          if(total == data) {
            // console.log(json[total]);
            json[total] = Number(json[total]);
            expend += json[total];
            // console.log(expend);
          }
        })
      })

      // console.log(expend , json['upto_price']);
      var device_price = json['upto_price'] - expend;
      if(device_price >= 500 ) {
        
        localStorage.removeItem('device_price');
        localStorage.removeItem('b_name');
        localStorage.removeItem('p_name');

        localStorage.setItem('device_price', JSON.stringify(device_price));
        localStorage.setItem('b_name', json['product']);
        localStorage.setItem('p_name', json['series']);
        localStorage.setItem('totalArray', JSON.stringify(totalArray));
        this.router.navigate(['final-form']);
      }
      else {
        this.router.navigate(['final-error']);
        localStorage.setItem('model', this.model);
      }
    },err=>{
      console.log(err)
    });

  //   var json = {
  //     "modal": "Mi A1 4/64GB",
  //     "series": "Mi Series",
  //     "product": "Mi",
  //     "g_id": "757",
  //     "month": "1",
  //     "price": "0",
  //     "upto_price": 7500,
  //     "coupon": "100",
  //     "product_image": "product images/mi_a1.jpg",
  //     "display_touched_issue_discoloration": 3650,
  //     "front_camera": 1050,
  //     "back_camera": 1150,
  //     "volume_power_button": 650,
  //     "gps_wifi_issue": 2100,
  //     "finger_touch_issue": 1550,
  //     "battery_issue": 900,
  //     "charging_defect": 1200,
  //     "speaker": 600,
  //     "screen_broker": 3285,
  //     "box_missing": 700,
  //     "charger_missing": 400,
  //     "earphone_missing": 300,
  //     "excellent": 0,
  //     "good": 650,
  //     "average": 1400,
  //     "below_average": 2000,
  //     "coupon_code": "cash2000",
  //     "coupon_code2": "CRP300",
  //     "coupon_code3": "CHDP300",
  //     "coupon_code4": "HOLIDAY2000",
  //     "coupon_code5": "NEW2019",
  //     "coupon_code6": "WAP2000",
  //     "coupon_code7": "WXPQ2000S",
  //     "coupon_code8": "GHUBP300",
  //     "coupon_code9": "TIKTOK2000",
  //     "coupon_code10": "LP2000",
  //     "coupon_code1": "P300"
  // }


  }
}