import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverallConditionComponent } from './overall-condition.component';

describe('OverallConditionComponent', () => {
  let component: OverallConditionComponent;
  let fixture: ComponentFixture<OverallConditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverallConditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverallConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
