import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HelperService } from 'src/app/services/helper.service';
// import * as moment from 'moment';

@Component({
  selector: 'app-final-form',
  templateUrl: './final-form.component.html',
  styleUrls: ['./final-form.component.css']
})
export class FinalFormComponent implements OnInit {
  device_price: any;
  updated_device_price: any;
  phoneMatch: Boolean = true;
  hidePromo: Boolean = false;
  promo: any;
  promoButton: Boolean = true;
  form: FormGroup;
  coupon: any = [];
  model;
  promo_amount = 0;
  coupon_code = '';


  constructor(
    public api: ApiService,
    public router: Router,
    public helper: HelperService
  ) {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      address: new FormControl('', [Validators.required]),
      pincode: new FormControl('', [
        Validators.required,
        Validators.pattern('^4.*$'),
        Validators.minLength(6)
      ]),
      date: new FormControl(this.getDate(), [Validators.required]),
      mobile: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]{10}')
      ]),
      mobile1: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]{10}')
      ])
    });
  }

  ngOnInit() {
    this.model = localStorage.getItem('model');
    this.helper.showFooter();
    this.device_price = this.updated_device_price = Number(
      localStorage.getItem('device_price')
    );
  }

  getDate() {
    // Create new Date instance
    const date = new Date();

    // Add a day
    date.setDate(date.getDate() + 1);

    const formattedDate =
      date.getFullYear() +
      '-' +
      ('0' + (date.getMonth() + 1)).slice(-2) +
      '-' +
      ('0' + date.getDate()).slice(-2);

    // console.log(formattedDate);
    // console.log(
    //   moment()
    //     .add(1, 'days')
    //     .format('YYYY-MM-DD')
    // );
    // return moment()
    //   .add(1, 'days')
    //   .format('YYYY-MM-DD');
    return formattedDate;
  }

  getPromo() {
    if (this.promo.length > 0 || this.promo == '') {
      this.promoButton = false;
    }
  }

  applyPromo() {
    const formdata = new FormData();
    const form = {
      coupon_code: this.promo,
      g_id: localStorage.getItem('g_id')
    };
    Object.keys(form).forEach(i => {
      formdata.append(i, form[i]);
    });

    let count = 0;
    if (this.coupon.length == 0) {
      this.promoApi(formdata);
    } else {
      this.coupon.forEach((element, index) => {
        if (element == this.promo) {
          count += 1;
        }

        if (this.coupon.length - 1 == index) {
          if (count == 0) {
            this.promoApi(formdata);
          } else {
            alert('Coupon code already used by you!');
          }
        }
      });
    }
  }

  promoApi(formdata) {
    this.api.postPromo(formdata).subscribe(
      (res: any) => {
        if (res === 'n') {
          alert('Invalid coupon code');
        } else {
          this.coupon.push(this.promo);
          const num = parseFloat(res);
          this.promo_amount = num;
          this.updated_device_price = this.device_price + num;
          this.hidePromo = false;
          this.coupon_code = this.promo;
          this.promo = '';
          // alert(
          //   'Coupon code successfully applied! Rs.' +
          //     res +
          //     ' added to phone value'
          // );
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  checkPhoneMatch() {
    // console.log(this.form.value['mobile1'], this.form.value['mobile'] , form);
    if (this.form.value['mobile1'] == this.form.value['mobile']) {
      this.phoneMatch = false;
      // console.log(this.phoneMatch);
    } else {
      // console.log(this.phoneMatch);

      this.phoneMatch = true;
    }
  }

  
  submit() {
    const form = this.form.value;

    form['total'] = this.updated_device_price;
    form['totalArray'] = JSON.parse(localStorage.getItem('totalArray'));
    form['b_name'] = localStorage.getItem('b_name');
    form['p_name'] = localStorage.getItem('p_name');
    form['p_version'] = localStorage.getItem('modal');
    form['bill'] = localStorage.getItem('bill');
    form['coupon_code'] = this.coupon_code;
    form['coupon_amount'] = this.promo_amount;
    console.log(form);

    const formdata = new FormData();
    Object.keys(form).forEach(i => {
      formdata.append(i, form[i]);
    });

    this.api.postPersonalData(formdata).subscribe(
      res => {
        // console.log(res);
        // if (res && res['status']) {
        this.router.navigate(['thank-you']);
        // } else {
        // alert('Sorry! Your request could not be completed.');
        // }
      },
      err => {
        console.log(err);
        alert('Sorry! Your request could not be completed.');
      }
    );
  }

  tabHandler(e) {
    if (e.which !== 9) {
        return false;
    }
  }
}
