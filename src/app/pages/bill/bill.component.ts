import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.css']
})
export class BillComponent implements OnInit {
  billsArray: any = [];
  model;
  constructor(private route: ActivatedRoute, public router: Router, public helper: HelperService) {
    this.route.params.subscribe((params: any) => {
      // console.log(params);

      if (params && params.model) {
        console.log(params.model , localStorage.getItem('modal'))
        if(params.model == localStorage.getItem('modal')) {
          this.model = params.model;
          // console.log('Series name: ', helper.replaceDash(params.model));
          localStorage.removeItem('modal');
          localStorage.setItem('modal',helper.replaceDash(params.model));
        }
        else {
          this.router.navigate(['404']);
        }
      } else {
        console.error('No series received in URL');
      }
    });
  }

  ngOnInit() {
    this.helper.showFooter();

    this.initArray();
  }

  initArray() {
    this.billsArray = [
      {
        id: 1,
        bill: 'Bill 0-3 Months',
        invoice: 'Invoice is mandatory hard/soft copy',
        name: 'bill_3',
        value: 'bill_0_to_3',
        src: 'assets/img/bill.png',
        class:'',
        status:false
      },
      {
        id: 2,
        bill: 'Bill 3-6 Months',
        invoice: 'Invoice is mandatory hard/soft copy',
        name: 'bill_6',
        value: 'bill_3_to_6',
        src: 'assets/img/bill.png',
        class:'',
        status:false
      },
      {
        id: 3,
        bill: 'Bill 6-11 Months',
        invoice: 'Invoice is mandatory hard/soft copy',
        name: 'bill_6_up',
        value: 'bill_6_to_11',
        src: 'assets/img/bill.png',
        class:'',
        status:false
      },
      {
        id: 4,
        bill: 'Bill >11 Months',
        invoice: '  ',
        name: 'bill_11_up',
        value: 'bill_11_above',
        src: 'assets/img/bill.png',
        class:'',
        status:false
      }
    ]
  }

  getBill(data) {
    this.initArray();
    data.class = 'image-checkbox1-bill';
    data.status = true;

    // console.log(this.billsArray)

    localStorage.removeItem('bill');
    localStorage.setItem('bill', JSON.stringify(data.id));
    localStorage.setItem('model', this.model);
    this.router.navigate(['conditions']);
  }
}