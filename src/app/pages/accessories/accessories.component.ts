import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-accessories',
  templateUrl: './accessories.component.html',
  styleUrls: ['./accessories.component.css']
})
export class AccessoriesComponent implements OnInit {
  accessoriesArray: any = [];
  model;

  constructor(public router: Router, public helper: HelperService) { }

  ngOnInit() {
    this.helper.hideFooter();
    this.model = localStorage.getItem('model');
    this.accessoriesArray = [
      {
        name: 'Original Charger',
        class: '',
        src: 'assets/img/original-charger.png',
        alt: 'charger_missing',
        status: true
      },
      {
        name: 'Earphone',
        class: '',
        src: 'assets/img/earphone.png',
        alt: 'earphone_missing',
        status: true
      },
      {
        name: 'Box',
        class: '',
        src: 'assets/img/phone-box.png',
        alt: 'box_missing',
        status: true
      }
    ]
  }

  getAccessory(data) {
    if(data.status == false) 
      data.status = true;
    else if(data.status == true)
      data.status = false;
  }

  navigate() {
    var accessory = [];
    this.accessoriesArray.forEach((element,index) => {
      if(element.status == false) {
        accessory.push(element.alt);
      }

      if(index == this.accessoriesArray.length - 1) {
        // console.log(accessory);
        
        localStorage.removeItem('accessory');

        var arr = ['charger_missing', 'box_missing', 'earphone_missing'];
        
        let missing = arr.filter(item => accessory.indexOf(item) < 0);
        // console.log(missing);

        localStorage.setItem('accessory',JSON.stringify(missing));
        localStorage.setItem('model' , this.model);
        this.router.navigate(['overall-conditions']);
      }
    });
  }
}
