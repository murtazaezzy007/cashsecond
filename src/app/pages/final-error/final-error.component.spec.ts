import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinalErrorComponent } from './final-error.component';

describe('FinalErrorComponent', () => {
  let component: FinalErrorComponent;
  let fixture: ComponentFixture<FinalErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinalErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinalErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
