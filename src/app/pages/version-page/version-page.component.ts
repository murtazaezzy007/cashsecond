import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-version-page',
  templateUrl: './version-page.component.html',
  styleUrls: ['./version-page.component.css']
})
export class VersionPageComponent implements OnInit {
  versions: any = [];

  constructor(private route: ActivatedRoute, public api: ApiService, private helper: HelperService, public router: Router) {
    this.route.params.subscribe((params: any) => {
      // console.log(params);

      if (params && params.series) {
        // console.log('Series name: ', params.series);

        this.api.getModel({series: this.helper.replaceDash(params.series)}).subscribe(res=>{          
          if(res == 'no') {
            this.router.navigate(['404'])            
          }
          else {
            localStorage.removeItem('modal');
            this.versions = res;
          }
        },err=>{
          console.log(err);
        })
      } else {
        console.error('No series received in URL');
      }
    });
  }

  ngOnInit() {
    
  }

  saveImage(x) {
    localStorage.removeItem('product_image');
    localStorage.setItem('product_image',x.product_image);
    localStorage.removeItem('modal');
    localStorage.setItem('modal',this.helper.replaceSpaces(x.modal));
  }
}
