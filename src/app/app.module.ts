import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { NotfoundComponent } from './pages/error/notfound/notfound.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';
import { BillComponent } from './pages/bill/bill.component';
import { IssuesComponent } from './pages/issues/issues.component';
import { AccessoriesComponent } from './pages/accessories/accessories.component';
import { ConditionComponent } from './pages/condition/condition.component';
import { OverallConditionComponent } from './pages/overall-condition/overall-condition.component';
import { FinalFormComponent } from './pages/final-form/final-form.component';
import { FinalErrorComponent } from './pages/final-error/final-error.component';
import { SeriesPageComponent } from './pages/series-page/series-page.component';
import { VersionPageComponent } from './pages/version-page/version-page.component';
import { HttpClientModule ,HTTP_INTERCEPTORS} from '@angular/common/http';
import { ThankyouComponent } from './pages/thankyou/thankyou.component';

import { ConfirmEqualValidatorDirective } from './directives/confirm-equal-validator.directive';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { ContactComponent } from './pages/contact/contact.component';
// import { NgProgressModule } from '@ngx-progressbar/core';

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import {NgxMaskModule} from 'ngx-mask';
import { MatchHeightDirective } from './match-height.directive';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotfoundComponent,
    HeaderComponent,
    FooterComponent,
    ProgressBarComponent,
    BillComponent,
    IssuesComponent,
    AccessoriesComponent,
    ConditionComponent,
    OverallConditionComponent,
    FinalFormComponent,
    FinalErrorComponent,
    SeriesPageComponent,
    VersionPageComponent,
    ThankyouComponent,
    ConfirmEqualValidatorDirective,
    AboutUsComponent,
    ContactComponent,
    MatchHeightDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    // NgProgressModule.forRoot(),
    LoadingBarHttpClientModule,
    LoadingBarRouterModule,
    NgxMaskModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
    // NgProgressHttpModule,
    // NgProgressRouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
