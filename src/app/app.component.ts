import { Component } from '@angular/core';
import { RouterEvent, NavigationStart, NavigationEnd, NavigationCancel, NavigationError, Router } from '@angular/router';
// import { NgProgress } from '@ngx-progressbar/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular7';

  constructor(
    private router: Router,
    // public ngProgress: NgProgress
  ) {
    // this.router.events.subscribe((event: RouterEvent) => {
    //   // console.log(event)
    //   this.navigationInterceptor(event);
    // });
  }

  navigationInterceptor(event: RouterEvent): void {
    // if (event instanceof NavigationStart) {
    //   console.log("start")
    //   this.ngProgress.start();
    // }
    // if (event instanceof NavigationEnd) {
    //   this.ngProgress.done();
    // }

    // // Set loading state to false in both of the below events to hide the spinner in case a request fails
    // if (event instanceof NavigationCancel) {
    //   this.ngProgress.done();
    // }
    // if (event instanceof NavigationError) {
    //   this.ngProgress.done();
    // }
  }
}
