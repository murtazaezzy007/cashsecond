import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private apiUrl = environment.url;

  constructor(public http: HttpClient) { }

  getSeries(seriesId) {
    return this.http.get(this.apiUrl+'series.php', {params: seriesId});
  }
  
  getModel(model) {
    return this.http.get(this.apiUrl+'model.php', {params: model});
  }
  
  getSingleModel(model) {
    return this.http.get(this.apiUrl+'singlemodel.php', {params: model});
  }
  
  postPersonalData(data) {
    return this.http.post(this.apiUrl+'personal_details.php',  data);
  }
  
  postPromo(data) {
    return this.http.post(this.apiUrl+'coupon.php',  data);
  }
  
  postContactData(data) {
    return this.http.post(this.apiUrl+'contact.php',  data);
  }
}
