import { Injectable, Renderer2 } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor() { }

  hideFooter() {
    document.getElementById('footer').classList.add('hidden-xs');
  }

  showFooter() {
    document.getElementById('footer').classList.remove('hidden-xs');
  }

  getImage() {
    return 'assets/admin/'+localStorage.getItem('product_image');
  }

  replaceSpaces(key) {
    var data = key.replace(/\s+/g,"-");
    return data.replace(/\//g, "_");
  }

  replaceDash(key) {
    var data =  key.replace(/-/g, " ");
    return data.replace("_", "/");
  }
}
